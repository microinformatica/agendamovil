package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AgendaDBHelper  extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = " , ";
    private static final String SQL_CREATE_TABLE = " CREATE TABLE " +
     DefinirTabla.Contacto.TABLE_NAME + " ("  +
     DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY, " +
     DefinirTabla.Contacto.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
     DefinirTabla.Contacto.COLUMN_NAME_DIRECCION + TEXT_TYPE + COMMA_SEP +
     DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1 + TEXT_TYPE + COMMA_SEP +
     DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2 + TEXT_TYPE + COMMA_SEP +
     DefinirTabla.Contacto.COLUMN_NAME_NOTA + TEXT_TYPE + COMMA_SEP +
     DefinirTabla.Contacto.COLUMN_NAME_FAVORITE + INTEGER_TYPE  + ")";

    private   static  final String  SQL_DELETE_CONTACTO = " drop table if exists " +
      DefinirTabla.Contacto.TABLE_NAME;
    public static  final int DATABASE_VERSION = 1;
    public static  final  String DATABASE_NAME ="agenda.db";

    public  AgendaDBHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    public AgendaDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
         db.execSQL(SQL_DELETE_CONTACTO);
         onCreate(db);
    }
}
