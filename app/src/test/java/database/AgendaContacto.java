package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContacto {
    private Context context;
    private  AgendaDBHelper mDbHelper;
    private SQLiteDatabase db;

    String[] columnsToRead = new String[]{
    DefinirTabla.Contacto._ID,
    DefinirTabla.Contacto.COLUMN_NAME_NOMBRE,
    DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1,
    DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2,
    DefinirTabla.Contacto.COLUMN_NAME_DIRECCION,
    DefinirTabla.Contacto.COLUMN_NAME_NOTA,
    DefinirTabla.Contacto.COLUMN_NAME_FAVORITE

    };
    public AgendaContacto(Context context){
        this.context = context;
        mDbHelper = new AgendaDBHelper(this.context);
    }
    public void openDatabase(){

        db = mDbHelper.getWritableDatabase();
    }


    public  long  insertContacto(Contacto c){
        ContentValues values = new ContentValues() ;
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE,
                c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1,
                c.getTelefono1());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2,
                c.getTelefono2());

        values.put(DefinirTabla.Contacto.COLUMN_NAME_DIRECCION,
        c.getDireccion());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOTA,
        c.getNotas());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_FAVORITE,
        c.getFavorite());
        long id = db.insert(DefinirTabla.Contacto.TABLE_NAME,
                null,values);
        return id;
    }


    public long updateContacto(Contacto c,int id){

     ContentValues values = new ContentValues();

        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE,
                c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1,
                c.getTelefono1());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2,
                c.getTelefono2());

        values.put(DefinirTabla.Contacto.COLUMN_NAME_DIRECCION,
                c.getDireccion());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOTA,
                c.getNotas());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_FAVORITE,
                c.getFavorite());
        long cantidad = db.update(DefinirTabla.Contacto.TABLE_NAME,
                values,DefinirTabla.Contacto._ID + " = " +id,
                null);
               return  cantidad;
    }
   public  int deleteContacto(long id){
        return  db.delete(DefinirTabla.Contacto.TABLE_NAME,
                DefinirTabla.Contacto._ID + " =?",
                new String[]{String.valueOf(id)});
   }

   public Contacto leerContacto(Cursor cursor){
        Contacto c = new Contacto();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDireccion(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorite(cursor.getInt(6));
        return  c;

   }

   public Contacto getContacto(long id){
    SQLiteDatabase  db = mDbHelper.getReadableDatabase() ;
    Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
          columnsToRead,
            DefinirTabla.Contacto._ID + " = ?",
           new String[]{String.valueOf(id)},null,
            null,
            null,null
            );
     c.moveToFirst();
       Contacto contacto = leerContacto(c);
    return  contacto;

   }

   public ArrayList<Contacto> leerTodosContactos(){
        ArrayList<Contacto> todos = new ArrayList<Contacto>();
    Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
            columnsToRead,null,null,
            null,
           null,null,null);
     c.moveToFirst()  ;
     while(!c.isAfterLast()){
       Contacto contacto = leerContacto(c);
       todos.add(contacto);
       c.moveToNext();

     }
     c.close();
     return todos;
   }

}
